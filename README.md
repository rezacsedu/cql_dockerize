# Dockerize CQL evaluation engine

    Step 1: $ sudo git clone https://rezacsedu@bitbucket.org/rezacsedu/cql_dockerize.git 
    Step 2: $ cd cql_dockerize
    Step 3: $ sudo docker build -t jetty .
    Step 4: $ sudo docker run -p 8082:8080 -d jetty
    Step 5: Access the engine with GET/POST at <server_url>:8082/cql/evaluate
